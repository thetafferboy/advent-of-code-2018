# adventofcode.com 2018 day 2
# https://adventofcode.com/2018/day/2
# @thetafferboy

twos = 0
threes = 0

with open("input.txt", 'r') as box_id_input:
	box_ids = [line.strip() for line in box_id_input]

for box_id in box_ids:
	for character in box_id:
		if (box_id.count(character) == 2):
			twos = twos + 1
			break

	for character in box_id:
		if (box_id.count(character) == 3):
			threes = threes + 1
			break

print (f"Checksum is {twos*threes}")