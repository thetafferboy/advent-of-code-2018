# adventofcode.com 2018 day 3
# https://adventofcode.com/2018/day/3
# @thetafferboy

the_grid = [[0 for y in range(1001)] for x in range(1001)]


def ProcessLine(line_input):
	coords = line_input.split("@ ")[1]
	x_coord = int(coords.split(",")[0])
	y_coord = coords.split(",")[1]
	y_coord = int(y_coord.split(":")[0])

	sizes = line_input.split("x")[0]
	x_size = int(sizes.split(": ")[1])
	y_size = int(line_input.split("x")[1])

	DrawSquare(x_coord, y_coord, x_size, y_size)

def DrawSquare(x_coord, y_coord, x_size, y_size):
	current_x = x_coord
	current_y = y_coord

	for y in range(y_size):
		for x in range(x_size):
			the_grid[current_x][current_y] += 1
			current_x +=1
		current_x = x_coord
		current_y += 1

def CheckGrid():
	current_x = 0
	current_y = 0
	running_total = 0

	for y in range(1001):
		for x in range(1001):
			current_value = the_grid[current_x][current_y]
			if (current_value > 1):
				running_total += 1
			current_x +=1
		current_x = 0
		current_y += 1
	print(f"Total overlapping coords: {running_total}")


with open("input.txt", 'r') as input_file:
	input_lines = [line.strip() for line in input_file]
	for input_line in input_lines:
		ProcessLine(input_line)
	CheckGrid()
print("Done")

