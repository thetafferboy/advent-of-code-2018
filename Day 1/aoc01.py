# adventofcode.com 2018 day 1
# https://adventofcode.com/2018/day/1
# @thetafferboy

def ReadInput(filename):
	with open(filename, 'r') as frequencyinput:
		frequencies = [line.strip() for line in frequencyinput]
	ProcessFrequency(frequencies)

def ProcessFrequency(adjustments):
	frequency = 0
	for adjustment in adjustments:
		frequency = frequency + int(adjustment)
	print ("Final frequency: " + str(frequency))

ReadInput("input.txt")